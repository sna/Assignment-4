// Implement a binary search tree based on the tree sort example, also shown in
// class on Wednesday, September 28, 2022.  Make sure you use a comparable type
// for the node data (e.g., int or string).
// In addition, make sure you implement (at least) the following BST
// operations:
//    i. insert
//   ii. member/find
//  iii. isEmpty
//  iv. inOrder traversal (returns a slice of the tree elements that can be
// used to output the tree data)
// Optionally, you can implement the other 2 traversal algorithms, as well as
// the delete operation.
// Write a simple program to test/demonstrate using such a BST.
package main

import (
	"fmt"
)

// Node ...
type Node struct {
	val   int
	left  *Node
	right *Node
}

// BST is a Binary Search Tree
type BST struct {
	root *Node
}

func makeBST() *BST {
	n := BST{}
	return &n
}

func (t *BST) insert(value int) {
	if t.root == nil {
		t.root = &Node{val: value}
	} else {
		t.root.insert(value)
	}
}

// Node searches left, Node search right.
func (n *Node) insert(value int) {
	if n.val == value {
		fmt.Println("Value must be distinct for a BST")
		return
	}
	if n.val > value {
		if n.left == nil {
			n.left = &Node{val: value}
		} else {
			n.left.insert(value)
		}
	} else {
		if n.right == nil {
			n.right = &Node{val: value}
		} else {
			n.right.insert(value)
		}
	}

}

func (t *BST) find(value int) bool {
	return t.root.find(value)
}

func (n *Node) find(value int) bool {
	if n == nil {
		return false
	}
	if value < n.val {
		return n.left.find(value)
	}
	if value > n.val {
		return n.right.find(value)
	}
	return true
}

func (t *BST) isEmpty() bool {
	return t.root == nil
}

func (t *BST) inOrder() []int {
	s := make([]int, 0)
	t.root.inOrder(&s)
	return s
}

func (n *Node) inOrder(s *[]int) {
	if n == nil {
		return // base case
	}
	n.left.inOrder(s)
	*s = append(*s, n.val)
	n.right.inOrder(s)
}

func testEq(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	fmt.Printf("Expected %v, Got %v, ", b, a)
	return true
}

func verbose(b bool) {
	if b {
		fmt.Println("Ok? Yes!")
	} else {
		fmt.Println("Ok? No! Doom! Doom! Doom!")
	}
}

func main() {
	var t *BST = makeBST()
	t.insert(8)
	t.insert(4)
	t.insert(10)
	t.insert(2)
	t.insert(6)
	t.insert(1)
	t.insert(3)
	t.insert(5)
	t.insert(7)
	t.insert(9)

	inOrderTraversal := t.inOrder()
	expectedInOrderTraversal := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	fmt.Printf("t: inorder traversal: ")
	verbose(testEq(inOrderTraversal, expectedInOrderTraversal))

	fmt.Printf("t: find(5) = %t, ", t.find(5))
	verbose(t.find(5) == true)

	fmt.Printf("t: find(11) = %t, ", t.find(11))
	verbose(t.find(11) == false)

	fmt.Printf("t: BST is empty? %t, ", t.isEmpty())
	verbose(t.isEmpty() == false)

	var t2 *BST = makeBST()
	t2.insert(40)
	t2.insert(20)
	t2.insert(10)
	t2.insert(5)
	t2.insert(30)
	t2.insert(50)
	t2.insert(60)
	t2.insert(67)
	t2.insert(78)

	inOrderTraversal2 := t2.inOrder()
	expectedInOrderTraversal2 := []int{5, 10, 20, 30, 40, 50, 60, 67, 78}
	fmt.Printf("t2: inorder traversal: ")
	verbose(testEq(inOrderTraversal2, expectedInOrderTraversal2))

	fmt.Printf("t2: find(78) = %t, ", t2.find(78))
	verbose(t2.find(78) == true)

	fmt.Printf("t2: find(55) = %t, ", t2.find(55))
	verbose(t2.find(55) == false)

	fmt.Printf("t2: BST is empty? %t, ", t2.isEmpty())
	verbose(t2.isEmpty() == false)

	var t3 *BST = makeBST()
	fmt.Printf("t3: BST is empty? %t, ", t3.isEmpty())
	verbose(t3.isEmpty() == true)

}
