// Implement a directed or undirected graph using an adjacency matrix instead
// of the adjacency set implementation of a digraph shown, in class on
// Wednesday, September 28, 2022.
// Write a simple program to test/demonstrate using such an graph (you can use
// the test code from the adjacency set implementation of a digraph referenced
// above).
// HINT: Use a 2-D array of booleans.
package main

import (
	"fmt"
)

// Graph ...
type Graph struct {
	adjMatrix [][]bool
	codec     map[string]int
	counter   int
}

func (g *Graph) addMapping(item string) {
	if _, found := g.codec[item]; !found {
		g.codec[item] = g.counter
		g.counter++
	}
}

func (g *Graph) addEdge(from, to string) {
	g.addMapping(from)
	g.addMapping(to)
	if value, found := g.codec[from]; found {
		if value2, found2 := g.codec[to]; found2 {
			g.adjMatrix[value][value2] = true
		}
	}
}

func (g *Graph) hasEdge(from, to string) bool {
	if _, found := g.codec[from]; !found {
		return false
	}
	if _, found := g.codec[to]; !found {
		return false
	}
	return g.adjMatrix[g.codec[from]][g.codec[to]]
}

func makeGraph(numberOfVerticies int) *Graph {
	g := Graph{}
	g.adjMatrix = make([][]bool, numberOfVerticies)
	for i := range g.adjMatrix {
		g.adjMatrix[i] = make([]bool, numberOfVerticies)
	}
	g.codec = make(map[string]int)
	return &g
}

func Ok(a, b bool) string {
	if a == b {
		return "Ok!"
	} else {
		return "Doom! Doom! Doom!"
	}
}

func main() {
	var g *Graph = makeGraph(4)
	g.addEdge("a", "b")
	g.addEdge("c", "d")
	g.addEdge("a", "d")
	g.addEdge("d", "a")
	fmt.Printf("Expected %t, Got %t, %s\n", true,  g.hasEdge("a", "b"), Ok(true,  g.hasEdge("a", "b")))
	fmt.Printf("Expected %t, Got %t, %s\n", true,  g.hasEdge("c", "d"), Ok(true,  g.hasEdge("c", "d")))
	fmt.Printf("Expected %t, Got %t, %s\n", true,  g.hasEdge("a", "d"), Ok(true,  g.hasEdge("a", "d")))
	fmt.Printf("Expected %t, Got %t, %s\n", true,  g.hasEdge("d", "a"), Ok(true,  g.hasEdge("d", "a")))
	fmt.Printf("Expected %t, Got %t, %s\n", false, g.hasEdge("x", "b"), Ok(false, g.hasEdge("x", "b")))
	fmt.Printf("Expected %t, Got %t, %s\n", true,  g.hasEdge("c", "d"), Ok(true,  g.hasEdge("c", "d")))
	fmt.Printf("Expected %t, Got %t, %s\n", false, g.hasEdge("d", "c"), Ok(false, g.hasEdge("d", "c")))
	fmt.Printf("Expected %t, Got %t, %s\n", false, g.hasEdge("x", "d"), Ok(false, g.hasEdge("x", "d")))
	fmt.Printf("Expected %t, Got %t, %s\n", false, g.hasEdge("d", "x"), Ok(false, g.hasEdge("d", "x")))
}
